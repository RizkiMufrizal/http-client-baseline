package org.rizki.mufrizal.http.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.rizki.mufrizal.http.client.configuration.HttpComponentExecution;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@Slf4j
class HttpClientApplicationTests {

    @Test
    void contextLoads() throws Exception {
        Map<String, Object> stringObjectMap = new HashMap<>();
        stringObjectMap.put("Message", "Hello World");

        Map<String, String> stringStringMap = new HashMap<>();
        stringStringMap.put("Authorization", "Bearer " + Base64.getEncoder().encodeToString("admin:admin".getBytes()));

        HttpComponentExecution<SampleEntity> httpComponentExecution = new HttpComponentExecution<>("POST", "https://10.197.19.105:8065/test");
        SampleEntity sampleEntity = httpComponentExecution.executeJson(30000, 30000, stringObjectMap, stringStringMap, SampleEntity.class);
        log.info("httpComponentEntity {}", new ObjectMapper().writeValueAsString(sampleEntity));
    }

}

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
class SampleEntity {
    @JsonProperty("details")
    private Detail detail;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
class Detail {
    @JsonProperty("msgId")
    private String msgId;
}