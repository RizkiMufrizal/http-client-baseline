package org.rizki.mufrizal.http.client.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.util.Map;

@Slf4j
public class HttpComponentExecution<T> extends HttpUriRequestBase {

    public HttpComponentExecution(String method, String url) {
        super(method, URI.create(url));
    }

    public T executeJson(int connectTimeout, int activeTimeout, Object requestBody, Map<String, String> headers, Class<T> tClass) throws Exception {
        headers.entrySet().parallelStream().forEach(header -> this.addHeader(header.getKey(), header.getValue()));
        ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        if (requestBody != null) {
            try {
                this.setEntity(new StringEntity(objectMapper.writeValueAsString(requestBody), ContentType.APPLICATION_JSON));
            } catch (JsonProcessingException e) {
                StringWriter stringWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stringWriter));
                log.error("Exception {}", e.getMessage());
                log.error("Exception {}", stringWriter);
                throw new RuntimeException(e);
            }
        }

        log.info("Request Url {}", this.getUri());
        log.info("Request Method {}", this.getMethod());
        log.info("Request Header {}", objectMapper.writeValueAsString(this.getHeaders()));
        log.info("Request Body {}", objectMapper.writeValueAsString(requestBody));

        try (CloseableHttpClient closeableHttpClient = HttpComponentConfiguration.config(connectTimeout, activeTimeout)) {
            long t1 = System.nanoTime();
            return closeableHttpClient.execute(this, response -> {
                long t2 = System.nanoTime();
                String responseBody = EntityUtils.toString(response.getEntity());
                T tObject = objectMapper.readValue(responseBody, tClass);

                log.info(String.format("Received response for %s in %.1fms", this.getRequestUri(), (t2 - t1) / 1e6d));
                log.info("Response Header {}", objectMapper.writeValueAsString(response.getHeaders()));
                log.info("Response Http Status {}", response.getCode());
                log.info("Response Http Message {}", response.getReasonPhrase());
                log.info("Response Http Version {}", response.getVersion());
                log.info("Response Body {}", responseBody);
                log.info("Response Body Object {}", objectMapper.writeValueAsString(tObject));
                return tObject;
            });
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            log.error("Exception {}", e.getMessage());
            log.error("Exception {}", stringWriter);
            throw new RuntimeException(e);
        }
    }
}
